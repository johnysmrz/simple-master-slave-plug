#include <AmpMeter.h>
#include <Arduino.h>
#include <config.h>

#define CURRENT_SENSING_PIN A3
#define SSR_PIN 2

// 0W = <5
// 120W = 8000-9000 (8500 = 85) - 70.8
// 500W = 18500-20000 (19250 = 182,5) - 38.5
// 700W = 27000-27500 (27250 = 272.5) - 38.9
// 300W = 11000 (110) - 36.6
// 1630W = 68600 (686) - 42

int main() {
    init();
    Serial.begin(115200);
    pinMode(SSR_PIN, OUTPUT);

    AmpMeter channelA = AmpMeter(CURRENT_SENSING_PIN, CHANNEL_A_COEF);
    AmpMeter channelB = AmpMeter(CURRENT_SENSING_PIN, CHANNEL_B_COEF);

    unsigned long lastChange = 0;
    bool targetOutputState = false;

    unsigned char thresholdIndex = 0;
    unsigned int wattageTiers[3] = {WATTAGE_TIER_LOW, WATTAGE_TIER_MED, WATTAGE_TIER_HIGH};

    unsigned int leadingDelay = 1500;
    unsigned int trailingDelay = 2000;

    while (true) {
        channelA.loop();
        channelB.loop();

        if (channelA.ready()) {
            if (channelA.getWatt() >= wattageTiers[thresholdIndex]) {
                if (!targetOutputState) {
                    lastChange = millis();
                }
                targetOutputState = true;
            } else {
                if (targetOutputState) {
                    lastChange = millis();
                }
                targetOutputState = false;
            }

            if (targetOutputState && millis() - leadingDelay > lastChange) {
                digitalWrite(SSR_PIN, HIGH);
            } else if (millis() - trailingDelay > lastChange) {
                digitalWrite(SSR_PIN, LOW);
            }
        }
    }
}
