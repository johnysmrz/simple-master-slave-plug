#include <Arduino.h>

class AmpMeter {
   private:
    unsigned long currentAccumulator = 0;
    unsigned short iteration = 0;
    unsigned long lastTime = millis();

    unsigned int currentAmperes = 0;
    uint8_t currentSensingPin;
    int coeficient;
    bool newSampleReady = false;

   public:
    AmpMeter(unsigned char currentSensingPin, int coeficient);
    void loop();
    bool ready();
    unsigned int getCurrent();
    unsigned int getWatt();
};
