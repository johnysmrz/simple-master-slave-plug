#include <Arduino.h>
#include <AmpMeter.h>

#define AMP_METER_THRESHOLD 3

AmpMeter::AmpMeter(unsigned char currentSensingPin, int coeficient) {
    this->currentSensingPin = currentSensingPin;
    pinMode(this->currentSensingPin, INPUT);
    this->coeficient = coeficient;
    lastTime = millis();
};

void AmpMeter::loop() {
    if (millis() - this->lastTime > 1) {
        this->currentAccumulator += analogRead(this->currentSensingPin);
        if (this->iteration == 200) {
            this->currentAmperes = this->currentAccumulator / 200;
            this->iteration = 0;
            this->currentAccumulator = 0;
            this->newSampleReady = true;
        } else {
            this->iteration++;
        }
        this->lastTime = millis();
    }
};

bool AmpMeter::ready() {
    if (this->newSampleReady) {
        this->newSampleReady = false;
        return true;
    }
    return false;
};

unsigned int AmpMeter::getCurrent() {
    if (this->currentAmperes < AMP_METER_THRESHOLD) {
        return 0;
    }
    return this->currentAmperes;
};

unsigned int AmpMeter::getWatt() {
    return this->getCurrent() * this->coeficient;
};
